class Model:
    def __init__(self):
        self.text = "Hello"

    def getUpdatedText(self):
        if self.text == "Hello":
            self.text = "Hello World!"
        else:
            self.text = "Hello"

        return self.text
from PyQt5 import QtWidgets

from model.model import Model
from view.view import Ui_MainWindow


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        self.model = Model()

        self.setupConnections()

    def setupConnections(self):
        self.pushButton.pressed.connect(self.clickedButton)

    def clickedButton(self):
        text = self.model.getUpdatedText()
        self.label.setText(text)

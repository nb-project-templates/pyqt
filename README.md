# PyQt Template

Template for a PyQt application.

This README contains instructions for how to use this template using PyCharm, or any other IDE using the command line.

## Basic Instructions

1. Create a copy of this project template.
2. Change all the settings.
3. Design a view using Qt Designer
4. Generate the `UI_MainWindow` class using Py UIC.
5. Run the GUI by running the main script.

## Prerequisites

- Python 3: Programming language
  - Downloaded the latest version from [here](https://www.python.org/downloads/).
- Qt Designer: Application used to design the view for Qt applications.
  - Can be downloaded from [here](https://build-system.fman.io/qt-designer-download).

## Setting up

Either create a copy of this template, or copy this files from this project to an existing project.

### Python packages

All python packages required for this template are in the `requirements.txt` file.
They can be installed using the following command in the project root folder:

```bash
pip install -r requirements.txt
```

#### Setup Virtual Environment

If not copied the virtual environment from this template project, one can easily be created

A python virtual environment can easily be setup using the following command:

```bash
python3 -m venv venv
```

The virtual environment needs to be activated before it can be used.

On Windows, run:

```
tutorial-env\Scripts\activate.bat
```
On Unix or MacOS, run:

```bash
source venv/bin/activate
```

The packages in the `requirements.txt` file can be installed into the virtual environment using the command above.

#### Updating the `requirements.txt`

To update the `requirements.txt` file after installing more packages,
run the following command in the project root folder:

```bash
pip freeze > requirements.txt
```

## Creating View

In the view directory there are two files, `view.ui` and `view.py`.

The `view.ui` file can be created using Qt Designer, which is a useful application used to design Qt UIs.

The `view.py` file is generated from the UI, using pyuic.
This file contains the `Ui_MainWindow` class used to create the main window for the GUI.

### Creating UI

To create the UI, open Qt Designer and open the `view.ui` file in the view directory of the project.
Then edit the UI as fit for the GUI. When the changes are complete save them to the `view.ui` file.

Instructions on how to use Qt Designer can be found in the
[Qt Designer Manual](https://doc.qt.io/qt-5/qtdesigner-manual.html).

### Generating `Ui_MainWindow`

In order to use the view created the `Ui_MainWindow` class needs to be generated.
This is done by running Py UIC, using the following command in the project root folder:

```bash
python -m PyQt5.uic.pyuic view/view.ui -o view/view.py
```

## Running GUI

To run the GUI, simply run the `main.py` script, which can easily be done from any IDE.

To run the program from the command line run the following command in the project root folder:

```bash
python main.py
```

## Application

This template also provides the files necessary to create an executable on Windows,
or app on macOS, out of the GUI using PyInstaller.

If this is not something that is required, the `info` folder can be removed.
It can of course be added back when required.

### Info

The info folder contains additional files for the applications.

The `info/metadata.yaml` file contains metadata that will be added to the application/executable.
These should be updated with information about the project this template is used for.

The version number in the `info/metadata.yaml` file should be updated whenever there is a change in the version.

The `info/app.ico` file stores the icon that will be used for the application.

### Creating Application

#### Generate `metadata.py`

The metadata yaml files needs to be converted to a python file.
This can be done by running the following command in the project root folder:

```bash
create-version-file $FileName$ --outfile info/metadata.py
```

#### Generate Application

PyInstaller can then be used to generate the application. To do so run the following command in the project root folder:

```bash
-n=<ApplicationName> --windowed --version-file=info/metadata.py --icon=info/app.ico -y main.py
```

Replace <ApplicationName> with the name of the application.

This will generate the application in the dist directory.

The version should be updated if any significant changes are made to keep track of the progress.

## PyCharm

PyCharm is part of the JetBrains suite of IDE's, specific for Python.
It can be downloaded from [here](https://www.jetbrains.com/pycharm/).

### External Tools

You can add external tools to PyCharm that can be accessed and run from the Project explorer view.

To add external tools, open settings, and then go to Tools > External tools, 
and then you can click the `+` icon to add new external tools.

#### External Tools for Template

Go to settings, Tools > External Tools and add the following:

| Name                     | Description                                                    | Program                       | Arguments                                                                                                                    | Working directory |
|--------------------------|----------------------------------------------------------------|-------------------------------|------------------------------------------------------------------------------------------------------------------------------|-------------------|
| Qt Designer              | Open Qt Designer to create view                                | /Applications/Qt Designer.app |                                                                                                                              |                   |
| PyUIC                    | Generates `Ui_MainWindow` class from selected .ui file         | python3                       | -m PyQt5.uic.pyuic $FileDirRelativeToProjectRoot$/$FileName$ -o $FileDirRelativeToProjectRoot$/$FileNameWithoutExtension$.py | $ProjectFileDir$  |
| PyInstaller              | Builds application with the projects name of the selected file | pyinstaller                   | -n=$ProjectName$ --windowed --version-file=info/metadata.py --icon=info/app.ico -y $FileName$                                | $ProjectFileDir$  |
| PyInstaller Version File | Convert the selected metadata file to a .py file               | create-version-file           | $FileName$ --outfile info/metadata.py                                                                                        | $ProjectFileDir$  |

#### Install Python Packages

In order to use the external tools the python packages used in this template need to be installed globally.
To do this run the following command in the project root folder in a terminal not using the virtual environment:

```bash
pip install -r requirement.txt
```

#### Using External Tools

To use the external tools right-click the file to run the tool on in the `Project` view.
Go to `External Tools` towards the bottom and click to tool to run on the selected file.

## Migrate to PySide2

A downside of using PyQt5 is that it is available under a GPL or commercial licence.
PySide2 provides an alternative under the LGPL license.

Instructions on migrating a project from PyQt5 to PySide2 can be found
[here](https://www.geeksforgeeks.org/migrate-pyqt5-app-to-pyside2/).

## Further information

[The Python Tutorial](https://docs.python.org/3/tutorial/index.html) teaches the basics of Python.


The PyQt5 Documentation page cann be found [here](https://www.riverbankcomputing.com/static/Docs/PyQt5/)